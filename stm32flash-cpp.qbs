import qbs

CppApplication {
    consoleApplication: true

    name:"stm32flash"

    targetName : {
        if(qbs.buildVariant == "debug")
            return "stm32flashd"
        else
            return "stm32flash"
    }

    Group{
        name:"app"
        prefix:"src/"
        files:[
            "main.cpp",
            "init.cpp",
            "init.h",
            "stm32.h",
            "stm32.cpp",
            "utils.h",
            "utils.cpp",
            "serial.h",
            "serial_common.cpp",
            "applogger.hpp"
        ]
    }
    Group{
        condition: qbs.targetOS.contains("windows")
        name:"serial_w32"
        prefix:"src/"
        files:"serial_w32.cpp"
    }
    Group{
        condition: qbs.targetOS.contains("linux")
        name:"serial_linux"
        prefix:"src/"
        files:"serial_posix.cpp"
    }

    Group{
        name:"parsers"
        prefix:"src/parsers/"
        files:["*.h","*.cpp"]
    }

    Group{
        name:"log"
        prefix:"src/cpplog/"
        files:["cpplog.hpp"]
    }
    Group{
        name:"Console"
        prefix:"src/Console/"
        files:["*.h","*.cpp"]
    }
}
