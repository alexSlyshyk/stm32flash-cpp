/**************************************************************************************************
*     file     : applogger.hpp
*     created  : 06.03.2014
*     author   : Slyshyk Oleksiy ( alexSlyshyk@gmail.com )
**************************************************************************************************/

#ifndef APPLOGGER_HPP
#define APPLOGGER_HPP

#include <iostream>

#ifdef WIN32
#define __func__ __FUNCTION__
#endif

// This define must be before including "cpplog.hpp"
#define LOG_LEVEL(level, logger) CustomLogMessage(__FILE__, __func__, __LINE__, (level), logger).getStream()

#include "cpplog/cpplog.hpp"
#include "Console/console.h"

class ColoredLogger: public cpplog::BaseLogger
{
public:
    virtual bool sendLogMessage(cpplog::LogData* logData)
    {
        cpplog::helpers::fixed_streambuf* const sb = &logData->streamBuffer;
        if(logData->level == LL_INFO)
            {
                Console::SetFGColor(ColorWhite);
                std::cout.write(sb->c_str(), sb->length());
                std::cerr.flush();
            }

        else if(logData->level == LL_DEBUG)
            {
                Console::SetFGColor(ColorYellow);
                std::cerr.write(sb->c_str(), sb->length());
                Console::SetFGColor(ColorWhite);
                std::cerr.flush();
            }
        else if(logData->level == LL_TRACE)
            {
                Console::SetFGColor(ColorYellow);
                std::cerr.write(sb->c_str(), sb->length());
                Console::SetFGColor(ColorWhite);
                std::cerr.flush();
            }
        else if(logData->level == LL_WARN)
            {
                Console::SetFGColor(ColorMagenta);
                std::cerr.write(sb->c_str(), sb->length());
                Console::SetFGColor(ColorWhite);
                std::cerr.flush();
            }
        else
            {
                Console::SetFGColor(ColorRed);
                std::cerr.write(sb->c_str(), sb->length());
                Console::SetFGColor(ColorWhite);
                std::cerr.flush();
            }

        return true;
    }

};

class CustomLogMessage : public cpplog::LogMessage
{
public:
    CustomLogMessage(const char*  file, const char* function,
                     unsigned int line, cpplog::loglevel_t logLevel,
                     cpplog::BaseLogger &outputLogger)
    : cpplog::LogMessage(file, line, logLevel, outputLogger, false),
      m_function(function)
    {
        if(m_logData->level != LL_INFO)
            {
                m_logData->stream
                    <<"["<< shortLogLevelName(m_logData->level) <<"] ";
            }
    }

    ~CustomLogMessage()
    {
        InitLogMessage();
    }

    static const char* shortLogLevelName(cpplog::loglevel_t logLevel)
    {
        switch( logLevel )
        {
            case LL_TRACE: return "T";
            case LL_DEBUG: return "D";
            case LL_INFO:  return "I";
            case LL_WARN:  return "W";
            case LL_ERROR: return "E";
            case LL_FATAL: return "F";
            default:       return "O";
        };
    }

protected:
    virtual void InitLogMessage()
    {
        if(m_logData->level != LL_INFO)
            {
                m_logData->stream
                    << " ["
                    << m_logData->fileName << ":"
                    << m_function          << ":"
                    << m_logData->line
                    << "]";
            }

    }
private:
    const char *m_function;
};

#endif // APPLOGGER_HPP
